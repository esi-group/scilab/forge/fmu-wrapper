function builder_swig()
    src_swig_path = get_absolute_file_path("builder_swig.sce");
    src_c_path = fullpath(fullfile(src_swig_path, "..", "c"));
    src_cpp_path = fullpath(fullfile(src_swig_path, "..", "cpp"));

    CFLAGS = ilib_include_flag(src_c_path);
    CFLAGS = CFLAGS + ilib_include_flag(src_cpp_path);

    LDFLAGS = "";
    if (getos()<>"Windows") then
        LDFLAGS = LDFLAGS + " """ + src_cpp_path + "/libfmucpp.so""";
    else
        // Getting symbols
        if findmsvccompiler() <> "unknown" & haveacompiler() then
            LDFLAGS = LDFLAGS + " """ + src_cpp_path + "/libfmucpp.lib""";
        end
    end

    doc = xmlRead(src_swig_path + "/fmuswig_gateway.xml");
    names = xmlAsText(xmlXPath(doc, "//gateway/@name"));
    funs = xmlAsText(xmlXPath(doc, "//gateway/@function"));
    xmlDelete(doc);

    tbx_build_src(...
        funs, ..
        ["fmuswig_wrap.cxx"], ..
        "c", ..
        src_swig_path, ..
        "", ..
        LDFLAGS, ..
        CFLAGS, ..
        "", ..
        "", ..
        "fmuswig", ..
        "loader_invalid.sce");
    // loader.sce has already been generated by SWIG, use it !
        
    // build simpler mapping on the loader
    // TODO: should we generate simpler SWIG interface or Scilab macros on loader.sce ?
    
endfunction

builder_swig();
clear builder_swig; // remove builder_swig on stack
