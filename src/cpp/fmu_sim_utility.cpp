#include "fmu_wrapper.hxx"

extern "C" {
#include "dynlib_fmucpp_wrapper.h"

#include "fmiPlatformTypes.h"
#include "fmiFunctionTypes.h"
#include "fmiFunctions.h"

#include "fmi2TypesPlatform.h"
#include "fmi2FunctionTypes.h"
#include "fmi2Functions.h"

#include "scicos.h"
#include "scicos_block4.h"
#include "Scierror.h"
#include "scicos_malloc.h"
#include "scicos_free.h"
#include "lasterror.h"
#include "sciprint.h"
#include "fmu_sim_utility.h"
}

#include "UTF8.hxx"

#include <cstring>

// mask for negatedAlias or discrete variables
#define NEGATED_ALIAS   (1 << 0)
#define DISCRETE        (1 << 1)

// set start values for variables with initial="exact" (as default or preset)
int fmiSet_parameters(scicos_block *block, scicos_flag flag)
{
    uint32_t *sciRealRefs = NULL, *sciIntRefs = NULL, *sciBoolRefs = NULL;
    fmiReal *rpar = NULL;
    fmiInteger *ipar = NULL;
    int *sciBoolValues = NULL;
    size_t realRefsSize, intRefsSize, boolRefsSize;
    fmi2Status status2;
    fmi3Status status3;
    fmi2Status globalStatus2;
    fmi3Status globalStatus3;

    size_t i, j;
    workFMU *structFmu = *(workFMU **)(block->work);

    // real values
    sciRealRefs = Getuint32OparPtrs(block, 6); // real refs
    realRefsSize = GetOparSize(block, 6, 1) * GetOparSize(block, 6, 2); // real refs size
    rpar = GetRparPtrs(block); // real values
    // integer values
    sciIntRefs = Getuint32OparPtrs(block, 7);
    intRefsSize = GetOparSize(block, 7, 1) * GetOparSize(block, 7, 2);
    ipar = GetIparPtrs(block);
    // boolean values
    sciBoolRefs = Getuint32OparPtrs(block, 8);
    boolRefsSize = GetOparSize(block, 8, 1) * GetOparSize(block, 8, 2);
    sciBoolValues = Getint32OparPtrs(block, 9); // boolean values
    
    // real
    if (realRefsSize > 0)
    {
        fmi2Real* realValues = (fmi2Real*) scicos_malloc(realRefsSize/2 * sizeof(fmi2Real));

        // check negated alias and store referenceValues
        for (i = 0, j = realRefsSize/2; j < realRefsSize; i++, j++)
        {
            if (sciRealRefs[j] > 0)
            {
                // negated alias set to negative parameter value
                realValues[i] = -rpar[i];
            }
            else
            {
                realValues[i] = rpar[i];
            }

            FMU_SIM_PRINT("fmiSet_parameters real valueReference %d set to %lf\n", sciRealRefs[i], realValues[i]);
        }
        // set real values
        if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::ModelExchange)
        {
            status2 = structFmu->fmuME2->fmi2SetReal(structFmu->instance, sciRealRefs, i, realValues);
        }
        else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::CoSimulation)
        {
            status2 = structFmu->fmuCS2->fmi2SetReal(structFmu->instance, sciRealRefs, i, realValues);
        }
        else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::ModelExchange)
        {
            status3 = structFmu->fmuME3->fmi3SetFloat64(structFmu->instance, sciRealRefs, i, realValues, i);
        }
        else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::CoSimulation)
        {
            status3 = structFmu->fmuME3->fmi3SetFloat64(structFmu->instance, sciRealRefs, i, realValues, i);
        }
        else
        {
            // programming error
            assert(!"fmiSet_parameters: invalid version or type");
        }

        // store the highest possible error
        if (status2 > globalStatus2)
        {
            globalStatus2 = status2;
        }
        if (status3 > globalStatus3)
        {
            globalStatus3 = status3;
        }

        scicos_free(realValues);
    }
    // integers
    if (intRefsSize > 0)
    {
        fmi2Integer* intValues = (fmi2Integer*) scicos_malloc(intRefsSize/2 * sizeof(fmi2Integer));

        // check negated alias and store referenceValues
        for (i = 0, j = intRefsSize/2; j < intRefsSize; i++, j++)
        {
            if (sciIntRefs[j] > 0)
            {
                // negated alias set to negative parameter value
                intValues[i] = -ipar[i];
            }
            else
            {
                intValues[i] = ipar[i];
            }

            FMU_SIM_PRINT("fmiSet_parameters integer valueReference %d set to %lf\n", sciIntRefs[i], intValues[i]);
        }
        // set integer values
        if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::ModelExchange)
        {
            status2 = structFmu->fmuME2->fmi2SetInteger(structFmu->instance, sciIntRefs, i, intValues);
        }
        else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::CoSimulation)
        {
            status2 = structFmu->fmuCS2->fmi2SetInteger(structFmu->instance, sciIntRefs, i, intValues);
        }
        else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::ModelExchange)
        {
            status3 = structFmu->fmuME3->fmi3SetInt32(structFmu->instance, sciIntRefs, i, intValues, i);
        }
        else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::CoSimulation)
        {
            status3 = structFmu->fmuME3->fmi3SetInt32(structFmu->instance, sciIntRefs, i, intValues, i);
        }
        else
        {
            // programming error
            assert(!"fmiSet_parameters: invalid version or type");
        }

        // store the highest possible error
        if (status2 > globalStatus2)
        {
            globalStatus2 = status2;
        }
        if (status3 > globalStatus3)
        {
            globalStatus3 = status3;
        }

        scicos_free(intValues);
    }
    // boolean
    if (boolRefsSize > 0)
    {
        // pick the largest boolean type
        union sized_boolean_t
        {
            fmiBoolean b1;
            fmi2Boolean b2;
            fmi3Boolean b3;
        };
        
        fmi2Boolean* boolValues2 = (fmi2Boolean*) scicos_malloc(boolRefsSize/2 * sizeof(sized_boolean_t));
        fmi3Boolean* boolValues3 = (fmi3Boolean*) boolValues2;

        // check negated alias and store referenceValues
        for (i = 0, j = boolRefsSize/2; j < boolRefsSize; i++, j++)
        {
            if (sciBoolRefs[j] > 0)
            {
                // negated alias set to negative parameter value
                if (sciBoolValues[i] == 0)
                {
                    if (structFmu->version == workFMU::fmiV2)   
                    {
                        boolValues2[i] = fmi2True;
                    }
                    else if (structFmu->version == workFMU::fmiV3)   
                    {
                        boolValues3[i] = fmi3True;
                    }
                }
                else
                {
                    if (structFmu->version == workFMU::fmiV2)   
                    {
                        boolValues2[i] = fmi2False;
                    }
                    else if (structFmu->version == workFMU::fmiV3)   
                    {
                        boolValues3[i] = fmi3False;
                    }
                }
            }
            else
            {
                if (sciBoolValues[i] != 0)
                {
                    if (structFmu->version == workFMU::fmiV2)   
                    {
                        boolValues2[i] = fmi2True;
                    }
                    else if (structFmu->version == workFMU::fmiV3)   
                    {
                        boolValues3[i] = fmi3True;
                    }
                }
                else
                {
                    if (structFmu->version == workFMU::fmiV2)   
                    {
                        boolValues2[i] = fmi2False;
                    }
                    else if (structFmu->version == workFMU::fmiV3)   
                    {
                        boolValues3[i] = fmi3False;
                    }
                }
            }
            
            FMU_SIM_PRINT("fmiSet_parameters boolean valueReference %d set to %lf\n", sciBoolRefs[i], boolValues[i]);
        }
        // set boolean values
        if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::ModelExchange)
        {
            status2 = structFmu->fmuME2->fmi2SetBoolean(structFmu->instance, sciIntRefs, i, boolValues2);
        }
        else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::CoSimulation)
        {
            status2 = structFmu->fmuCS2->fmi2SetBoolean(structFmu->instance, sciIntRefs, i, boolValues2);
        }
        else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::ModelExchange)
        {
            status3 = structFmu->fmuME3->fmi3SetBoolean(structFmu->instance, sciIntRefs, i, boolValues3, i);
        }
        else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::CoSimulation)
        {
            status3 = structFmu->fmuME3->fmi3SetBoolean(structFmu->instance, sciIntRefs, i, boolValues3, i);
        }
        else
        {
            // programming error
            assert(!"fmiSet_parameters: invalid version or type");
        }

        // store the highest possible error
        if (status2 > globalStatus2)
        {
            globalStatus2 = status2;
        }
        if (status3 > globalStatus3)
        {
            globalStatus3 = status3;
        }

        scicos_free(boolValues2);
    }

    // report previous errors
    if (globalStatus2 > fmi2OK)
    {
        return globalStatus2;
    }
    if (globalStatus3 > fmi3OK)
    {
        return globalStatus3;
    }
    return 0;
}


// set input Values
int fmiSet_values(scicos_block *block, scicos_flag flag, int continuousMode, int *declaredTypes, int* blockTypes, size_t inpSize, uint32_t *sciInputRefs, void** from)
{
    fmiStatus status = fmiOK;
    fmi2Status status2 = fmi2OK;
    fmi3Status status3 = fmi3OK;
    fmiStatus globalStatus = fmiOK;
    fmi2Status globalStatus2 = fmi2OK;
    fmi3Status globalStatus3 = fmi3OK;

    size_t i, j, k;
    workFMU *structFmu = *(workFMU **)(block->work);
    if (structFmu == nullptr)
    {
        return fmiFatal;
    }

    for (i = 0, k = inpSize / 2, j = 0; k < inpSize; i++, j++, k++)
    {
        if (declaredTypes[i] == 1) // real
        {
            double* asDouble = ((double *) from[i]);
            double valueReal;

            if ((sciInputRefs[k] & NEGATED_ALIAS) == NEGATED_ALIAS) // negatedAlias
            {
                valueReal = -*asDouble;
            }
            else
            {
                valueReal = *asDouble;
            }

            if ((sciInputRefs[k] & DISCRETE) == DISCRETE) // discrete variable
            {
                if (structFmu->type == workFMU::ModelExchange && continuousMode)
                {
                    // do not set in continuous mode
                    continue;
                }
            }

            FMU_SIM_PRINT("fmiSet_values real valueReference %d set to %lf\n", sciInputRefs[j], valueReal);

            if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::ModelExchange)
            {
                status = structFmu->fmuME1->fmiSetReal((structFmu->instance), &sciInputRefs[j], 1, &valueReal);
            }
            else if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::CoSimulation)
            {
                status = structFmu->fmuCS1->fmiSetReal((structFmu->instance), &sciInputRefs[j], 1, &valueReal);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::ModelExchange)
            {
                status2 = structFmu->fmuME2->fmi2SetReal((structFmu->instance), &sciInputRefs[j], 1, &valueReal);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::CoSimulation)
            {
                status2 = structFmu->fmuCS2->fmi2SetReal((structFmu->instance), &sciInputRefs[j], 1, &valueReal);
            }
            else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::ModelExchange)
            {
                status3 = structFmu->fmuME3->fmi3SetFloat64((structFmu->instance), &sciInputRefs[j], 1, &valueReal, 1);
            }
            else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::CoSimulation)
            {
                status3 = structFmu->fmuCS3->fmi3SetFloat64((structFmu->instance), &sciInputRefs[j], 1, &valueReal, 1);
            }

            // store the highest possible error
            if (status > globalStatus)
            {
                globalStatus = status;
            }
            if (status2 > globalStatus2)
            {
                globalStatus2 = status2;
            }
            if (status3 > globalStatus3)
            {
                globalStatus3 = status3;
            }
        }
        else if ((declaredTypes[i] == 2) | (declaredTypes[i] == 5)) // integer or enumeration
        {
            int* asInt = (int *) from[i];
            int valueInt;

            if ((sciInputRefs[k] & NEGATED_ALIAS) == NEGATED_ALIAS) // negatedAlias
            {
                // mapped as double, convert
                if (blockTypes[i] == SCSREAL_N)
                {
                    double* d = (double*) from[i];
                    valueInt = (int) -*d;
                } else
                {
                    valueInt = -*asInt;
                }
            }
            else
            {
                // mapped as double, convert
                if (blockTypes[i] == SCSREAL_N)
                {
                    double* d = (double*) from[i];
                    valueInt = (int) *d;
                } else
                {
                    valueInt = *asInt;
                }
            }

            if ((sciInputRefs[k] & DISCRETE) == DISCRETE) // discrete variable
            {
                if (structFmu->type == workFMU::ModelExchange && continuousMode)
                {
                    // do not set in continuous mode
                    continue;
                }
            }
            
            FMU_SIM_PRINT("fmiSet_values integer valueReference %d set to %d\n", sciInputRefs[j], valueInt);
            
            if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::ModelExchange)
            {
                status = structFmu->fmuME1->fmiSetInteger((structFmu->instance), &sciInputRefs[j], 1, &valueInt);
            }
            else if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::CoSimulation)
            {
                status = structFmu->fmuCS1->fmiSetInteger((structFmu->instance), &sciInputRefs[j], 1, &valueInt);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::ModelExchange)
            {
                // 0 is not a valid enumeration value, do not set at re-initialization where everything is set to 0 first
                if ((flag == Initialization || flag == ReInitialization) && declaredTypes[i] == 5 && valueInt == 0)
                {
                    FMU_SIM_PRINT("fmiSet_values enumeration set to 0, ignored\n");
                }
                else
                {
                    status2 = structFmu->fmuME2->fmi2SetInteger((structFmu->instance), &sciInputRefs[j], 1, &valueInt);
                }
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::CoSimulation)
            {
                // 0 is not a valid enumeration value, do not set at re-initialization where everything is set to 0 first
                if ((flag == Initialization || flag == ReInitialization) && declaredTypes[i] == 5 && valueInt == 0)
                {
                    FMU_SIM_PRINT("fmiSet_values enumeration set to 0, ignored\n");
                }
                else
                {
                    status2 = structFmu->fmuCS2->fmi2SetInteger((structFmu->instance), &sciInputRefs[j], 1, &valueInt);
                }
            }
            else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::ModelExchange)
            {
                // 0 is not a valid enumeration value, do not set at re-initialization where everything is set to 0 first
                if ((flag == Initialization || flag == ReInitialization) && declaredTypes[i] == 5 && valueInt == 0)
                {
                    FMU_SIM_PRINT("fmiSet_values enumeration set to 0, ignored\n");
                }
                else
                {
                    status3 = structFmu->fmuME3->fmi3SetInt32((structFmu->instance), &sciInputRefs[j], 1, &valueInt, 1);
                }
            }
            else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::CoSimulation)
            {
                // 0 is not a valid enumeration value, do not set at re-initialization where everything is set to 0 first
                if ((flag == Initialization || flag == ReInitialization) && declaredTypes[i] == 5 && valueInt == 0)
                {
                    FMU_SIM_PRINT("fmiSet_values enumeration set to 0, ignored\n");
                }
                else
                {
                    status3 = structFmu->fmuCS3->fmi3SetInt32((structFmu->instance), &sciInputRefs[j], 1, &valueInt, 1);
                }
            }

            // store the highest possible error
            if (status > globalStatus)
            {
                globalStatus = status;
            }
            if (status2 > globalStatus2)
            {
                globalStatus2 = status2;
            }
            if (status3 > globalStatus3)
            {
                globalStatus3 = status3;
            }
        }
        else if (declaredTypes[i] == 3) // boolean, compatible with fmi2 as fmi2True == fmiTrue and fmi2False == fmiFalse
        {
            char* asChar = (char *) from[i];
            fmiBoolean v1Bool;
            fmi2Boolean v2Bool;
            fmi3Boolean v3Bool;

            if ((sciInputRefs[k] & NEGATED_ALIAS) == NEGATED_ALIAS) // negatedAlias
            {
                // mapped as double, convert
                if (blockTypes[i] == SCSREAL_N)
                {
                    double* d = (double*) asChar;
                    if (*d) {
                        v1Bool = fmiFalse;
                        v2Bool = fmi2False;
                        v3Bool = fmi3False;
                    } else {
                        v1Bool = fmiTrue;
                        v2Bool = fmi2True;
                        v3Bool = fmi3True;
                    }
                }
                else
                {
                    if (*asChar) {
                        v1Bool = fmiFalse;
                        v2Bool = fmi2False;
                        v3Bool = fmi3False;
                    } else {
                        v1Bool = fmiTrue;
                        v2Bool = fmi2True;
                        v3Bool = fmi3True;
                    }
                }
            }
            else
            {
                // mapped as double, convert
                if (blockTypes[i] == SCSREAL_N)
                {
                    double* d = (double*) asChar;
                    if (*d) {
                        v1Bool = fmiTrue;
                        v2Bool = fmi2True;
                        v3Bool = fmi3True;
                    } else {
                        v1Bool = fmiFalse;
                        v2Bool = fmi2False;
                        v3Bool = fmi3False;
                    }
                }
                else
                {
                    if (*asChar) {
                        v1Bool = fmiTrue;
                        v2Bool = fmi2True;
                        v3Bool = fmi3True;
                    } else {
                        v1Bool = fmiFalse;
                        v2Bool = fmi2False;
                        v3Bool = fmi3False;
                    }
                }
            }

            if ((sciInputRefs[k] & DISCRETE) == DISCRETE) // discrete variable
            {
                if (structFmu->type == workFMU::ModelExchange && continuousMode)
                {
                    // do not set in continuous mode
                    continue;
                }
            }
            
            FMU_SIM_PRINT("fmiSet_values boolean valueReference %d set to %d\n", sciInputRefs[j], v2Bool);
            
            if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::ModelExchange)
            {
                status = structFmu->fmuME1->fmiSetBoolean((structFmu->instance), &sciInputRefs[j], 1, &v1Bool);
            }
            else if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::CoSimulation)
            {
                status = structFmu->fmuCS1->fmiSetBoolean((structFmu->instance), &sciInputRefs[j], 1, &v1Bool);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::ModelExchange)
            {
                status2 = structFmu->fmuME2->fmi2SetBoolean((structFmu->instance), &sciInputRefs[j], 1, &v2Bool);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::CoSimulation)
            {
                status2 = structFmu->fmuCS2->fmi2SetBoolean((structFmu->instance), &sciInputRefs[j], 1, &v2Bool);
            }
            else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::ModelExchange)
            {
                status3 = structFmu->fmuME3->fmi3SetBoolean((structFmu->instance), &sciInputRefs[j], 1, &v3Bool, 1);
            }
            else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::CoSimulation)
            {
                status3 = structFmu->fmuCS3->fmi3SetBoolean((structFmu->instance), &sciInputRefs[j], 1, &v3Bool, 1);
            }

            // store the highest possible error
            if (status > globalStatus)
            {
                globalStatus = status;
            }
            if (status2 > globalStatus2)
            {
                globalStatus2 = status2;
            }
            if (status3 > globalStatus3)
            {
                globalStatus3 = status3;
            }
        }
        else if (declaredTypes[i] == 4) // string
        {
            int maxStringLen = GetInPortSize(block, i, 2);
            char* asCharPtr = (char *) from[i];
            asCharPtr[maxStringLen] = '\0'; // ensure string termination

            if ((sciInputRefs[k] & DISCRETE) == DISCRETE) // discrete variable
            {
                if (structFmu->type == workFMU::ModelExchange && continuousMode)
                {
                    // do not set in continuous mode
                    continue;
                }
            }
            
            FMU_SIM_PRINT("fmiSet_values string valueReference %d set to %s\n", sciInputRefs[j], asCharPtr);

            if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::ModelExchange)
            {
                status = structFmu->fmuME1->fmiSetString((structFmu->instance), &sciInputRefs[j], 1, &asCharPtr);
            }
            else if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::CoSimulation)
            {
                status = structFmu->fmuCS1->fmiSetString((structFmu->instance), &sciInputRefs[j], 1, &asCharPtr);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::ModelExchange)
            {
                status2 = structFmu->fmuME2->fmi2SetString((structFmu->instance), &sciInputRefs[j], 1, &asCharPtr);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::CoSimulation)
            {
                status2 = structFmu->fmuCS2->fmi2SetString((structFmu->instance), &sciInputRefs[j], 1, &asCharPtr);
            }
            else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::ModelExchange)
            {
                status3 = structFmu->fmuME3->fmi3SetString((structFmu->instance), &sciInputRefs[j], 1, &asCharPtr, 1);
            }
            else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::CoSimulation)
            {
                status3 = structFmu->fmuCS3->fmi3SetString((structFmu->instance), &sciInputRefs[j], 1, &asCharPtr, 1);
            }

            // store the highest possible error
            if (status > globalStatus)
            {
                globalStatus = status;
            }
            if (status2 > globalStatus2)
            {
                globalStatus2 = status2;
            }
            if (status3 > globalStatus3)
            {
                globalStatus3 = status3;
            }
        }
    }

    // report the highest possible error
    if (globalStatus != fmiOK)
    {
        return globalStatus;
    }
    if (globalStatus2 != fmi2OK)
    {
        return globalStatus2;
    }
    if (globalStatus3 != fmi3OK)
    {
        return globalStatus3;
    }
    return 0;
}

// get output Values
int fmiGet_values(scicos_block *block, int* declaredTypes, int* blockTypes, size_t outSize, uint32_t *sciOutputRefs, void** into)
{
    fmiStatus status = fmiOK;
    fmi2Status status2 = fmi2OK;
    fmi3Status status3 = fmi3OK;
    fmiStatus globalStatus = fmiOK;
    fmi2Status globalStatus2 = fmi2OK;
    fmi3Status globalStatus3 = fmi3OK;

    size_t i, j, k;
    workFMU *structFmu = *(workFMU **)(block->work);
    if (structFmu == nullptr)
    {
        return fmi2Fatal;
    }

    for (i = 0, k = outSize / 2, j = 0; k < outSize; i++, j++, k++)
    {
        if (declaredTypes[i] == 1) // Real
        {
            double *valueRealSci = (double *) into[i];
            double valueReal;
            if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::ModelExchange)
            {
                status = structFmu->fmuME1->fmiGetReal((structFmu->instance), &sciOutputRefs[j], 1, &valueReal);
            }
            else if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::CoSimulation)
            {
                status = structFmu->fmuCS1->fmiGetReal((structFmu->instance), &sciOutputRefs[j], 1, &valueReal);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::ModelExchange)
            {
                status2 = structFmu->fmuME2->fmi2GetReal((structFmu->instance), &sciOutputRefs[j], 1, &valueReal);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::CoSimulation)
            {
                status2 = structFmu->fmuCS2->fmi2GetReal((structFmu->instance), &sciOutputRefs[j], 1, &valueReal);
            }
            else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::ModelExchange)
            {
                status3 = structFmu->fmuME3->fmi3GetFloat64((structFmu->instance), &sciOutputRefs[j], 1, &valueReal, 1);
            }
            else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::CoSimulation)
            {
                status3 = structFmu->fmuCS3->fmi3GetFloat64((structFmu->instance), &sciOutputRefs[j], 1, &valueReal, 1);
            }

            FMU_SIM_PRINT("fmiGet_values real valueReference %d set to %lf\n", sciOutputRefs[j], valueReal);

            // store the highest possible error
            if (status > globalStatus)
            {
                globalStatus = status;
            }
            if (status2 > globalStatus2)
            {
                globalStatus2 = status2;
            }
            if (status3 > globalStatus3)
            {
                globalStatus3 = status3;
            }

            if ((sciOutputRefs[k] & NEGATED_ALIAS) == NEGATED_ALIAS) // negatedAlias
            {
                *valueRealSci = -valueReal;
            }
            else
            {
                *valueRealSci = valueReal;
            }
        }
        else if ((declaredTypes[i] == 2) | (declaredTypes[i] == 5)) // integer or enumeration
        {
            int valueInt;
            if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::ModelExchange)
            {
                status = structFmu->fmuME1->fmiGetInteger((structFmu->instance), &sciOutputRefs[j], 1, &valueInt);
            }
            else if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::CoSimulation)
            {
                status = structFmu->fmuCS1->fmiGetInteger((structFmu->instance), &sciOutputRefs[j], 1, &valueInt);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::ModelExchange)
            {
                status2 = structFmu->fmuME2->fmi2GetInteger((structFmu->instance), &sciOutputRefs[j], 1, &valueInt);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::CoSimulation)
            {
                status2 = structFmu->fmuCS2->fmi2GetInteger((structFmu->instance), &sciOutputRefs[j], 1, &valueInt);
            }
            else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::ModelExchange)
            {
                status3 = structFmu->fmuME3->fmi3GetInt32((structFmu->instance), &sciOutputRefs[j], 1, &valueInt, 1);
            }
            else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::CoSimulation)
            {
                status3 = structFmu->fmuCS3->fmi3GetInt32((structFmu->instance), &sciOutputRefs[j], 1, &valueInt, 1);
            }

            FMU_SIM_PRINT("fmiGet_values integer valueReference %d set to %d\n", sciOutputRefs[j], valueInt);

            // store the highest possible error
            if (status > globalStatus)
            {
                globalStatus = status;
            }
            if (status2 > globalStatus2)
            {
                globalStatus2 = status2;
            }
            if (status3 > globalStatus3)
            {
                globalStatus3 = status3;
            }

            if ((sciOutputRefs[k] & NEGATED_ALIAS) == NEGATED_ALIAS) // negatedAlias
            {
                if (blockTypes[i] == SCSREAL_N)
                {
                    // mapped as double
                    double* negated = (double*) into[i];
                    *negated = -valueInt;
                }
                else
                {
                    int* negated = (int *) into[i];
                    *negated = -valueInt;
                }
            }
            else
            {
                if (blockTypes[i] == SCSREAL_N)
                {
                    // mapped as double
                    double* asDouble = (double*) into[i];
                    *asDouble = valueInt;
                }
                else
                {
                    int* asInteger = (int *) into[i];
                    *asInteger = valueInt;
                }
            }
        }
        else if (declaredTypes[i] == 3) // boolean
        {
            fmiBoolean v1Bool = 0;
            fmi2Boolean v2Bool = 0;
            fmi3Boolean v3Bool = 0;
            int wideBool = 0;

            if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::ModelExchange)
            {
                status = structFmu->fmuME1->fmiGetBoolean((structFmu->instance), &sciOutputRefs[j], 1, &v1Bool);
                wideBool = v1Bool;
            }
            else if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::CoSimulation)
            {
                status = structFmu->fmuCS1->fmiGetBoolean((structFmu->instance), &sciOutputRefs[j], 1, &v1Bool);
                wideBool = v1Bool;
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::ModelExchange)
            {
                status2 = structFmu->fmuME2->fmi2GetBoolean((structFmu->instance), &sciOutputRefs[j], 1, &v2Bool);
                wideBool = v2Bool;
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::CoSimulation)
            {
                status2 = structFmu->fmuCS2->fmi2GetBoolean((structFmu->instance), &sciOutputRefs[j], 1, &v2Bool);
                wideBool = v2Bool;
            }
            else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::ModelExchange)
            {
                status3 = structFmu->fmuME3->fmi3GetBoolean((structFmu->instance), &sciOutputRefs[j], 1, &v3Bool, 1);
                wideBool = v3Bool;
            }
            else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::CoSimulation)
            {
                status3 = structFmu->fmuCS3->fmi3GetBoolean((structFmu->instance), &sciOutputRefs[j], 1, &v3Bool, 1);
                wideBool = v3Bool;
            }

            FMU_SIM_PRINT("fmiGet_values integer valueReference %d set to %d\n", sciOutputRefs[j], wideBool);

            // store the highest possible error
            if (status > globalStatus)
            {
                globalStatus = status;
            }
            if (status2 > globalStatus2)
            {
                globalStatus2 = status2;
            }
            if (status3 > globalStatus3)
            {
                globalStatus3 = status3;
            }

            if ((sciOutputRefs[k] & NEGATED_ALIAS) == NEGATED_ALIAS) // negatedAlias
            {
                if (blockTypes[i] == SCSREAL_N)
                {
                    // mapped as double
                    double* asDouble = (double*) into[i];
                    *asDouble = !wideBool;
                }
                else
                {
                    char* asChar = (char *) into[i];
                    *asChar = !wideBool;
                }
            }
            else
            {
                if (blockTypes[i] == SCSREAL_N)
                {
                    // mapped as double
                    double* asDouble = (double*) into[i];
                    *asDouble = wideBool;
                }
                else
                {
                    char* asChar = (char *) into[i];
                    *asChar = wideBool;
                }
            }
        }
        else if (declaredTypes[i] == 4) // string
        {
            fmiString str;

            int maxStringLen = GetInPortSize(block, i, 2);
            if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::ModelExchange)
            {
                status = structFmu->fmuME1->fmiGetString((structFmu->instance), &sciOutputRefs[j], 1, &str);
            }
            else if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::CoSimulation)
            {
                status = structFmu->fmuCS1->fmiGetString((structFmu->instance), &sciOutputRefs[j], 1, &str);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::ModelExchange)
            {
                status2 = structFmu->fmuME2->fmi2GetString((structFmu->instance), &sciOutputRefs[j], 1, &str);
            }
            else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::CoSimulation)
            {
                status2 = structFmu->fmuCS2->fmi2GetString((structFmu->instance), &sciOutputRefs[j], 1, &str);
            }
            else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::ModelExchange)
            {
                status3 = structFmu->fmuME3->fmi3GetString((structFmu->instance), &sciOutputRefs[j], 1, &str, 1);
            }
            else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::CoSimulation)
            {
                status3 = structFmu->fmuCS3->fmi3GetString((structFmu->instance), &sciOutputRefs[j], 1, &str, 1);
            }

            FMU_SIM_PRINT("fmiGet_values string valueReference %d set to %s\n", sciOutputRefs[j], str);

            // store the highest possible error
            if (status > globalStatus)
            {
                globalStatus = status;
            }
            if (status2 > globalStatus2)
            {
                globalStatus2 = status2;
            }
            if (status3 > globalStatus3)
            {
                globalStatus3 = status3;
            }

            char* asCharPtr = (char *) into[i];
            std::strncpy(asCharPtr, str, maxStringLen - 1);
            asCharPtr[maxStringLen] = '\0';
        }
    }

    // report the highest possible error
    if (globalStatus != fmiOK)
    {
        return globalStatus;
    }
    if (globalStatus2 != fmi2OK)
    {
        return globalStatus2;
    }
    if (globalStatus3 != fmi3OK)
    {
        return globalStatus3;
    }
    return 0;
}
void free_instance(scicos_block *block, int errorNumber, const char *functionName)
{
    workFMU *structFmu = *(workFMU **)(block->work);
    if (errorNumber)
    {
        Coserror("Error in simulation, returned by %s\n", functionName);
        set_block_error(errorNumber);
        
        std::wstring err = L"Error in FMU, " + scilab::UTF8::toWide(functionName) + L" function returned " + std::to_wstring(errorNumber) + L"\n";
        setLastErrorMessage(err.c_str());
    }

    if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::ModelExchange)
    {
        delete structFmu->fmuME1;
    }
    else if (structFmu->version == workFMU::fmiV1 && structFmu->type == workFMU::CoSimulation)
    {
        delete structFmu->fmuCS1;
    }
    else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::ModelExchange)
    {
        delete structFmu->fmuME2;
    }
    else if (structFmu->version == workFMU::fmiV2 && structFmu->type == workFMU::CoSimulation)
    {
        delete structFmu->fmuCS2;
    }
    else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::ModelExchange)
    {
        delete structFmu->fmuME3;
    }
    else if (structFmu->version == workFMU::fmiV3 && structFmu->type == workFMU::CoSimulation)
    {
        delete structFmu->fmuCS3;
    }

    scicos_free(structFmu);
    *(block->work) = NULL;
}
