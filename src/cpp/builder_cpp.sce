function builder_cpp()
    src_cpp_path = get_absolute_file_path("builder_cpp.sce");
    src_c_path = fullpath(fullfile(src_cpp_path, "..", "c"));

    CFLAGS = ilib_include_flag(src_cpp_path);
    CFLAGS = CFLAGS + " " + ilib_include_flag(src_c_path);

    LDFLAGS = "";

    if (getos()<>"Windows") then
        if ~isdir(SCI+"/../../share") then
            // Source version
            CFLAGS = CFLAGS + ilib_include_flag(SCI + "/modules/scicos_blocks/includes");
            CFLAGS = CFLAGS + ilib_include_flag(SCI + "/modules/scicos/includes");
            CFLAGS = CFLAGS + ilib_include_flag(SCI + "/modules/fileio/includes");
        else
            // Release version
            CFLAGS = CFLAGS + ilib_include_flag(SCI + "/../../include/scilab");
        end
    else
        CFLAGS = CFLAGS + ilib_include_flag(SCI + "/modules/scicos_blocks/includes");
        CFLAGS = CFLAGS + ilib_include_flag(SCI + "/modules/scicos/includes");
        CFLAGS = CFLAGS + ilib_include_flag(SCI + "/modules/fileio/includes");

        // Getting symbols
        if findmsvccompiler() <> "unknown" & haveacompiler() then
            LDFLAGS = LDFLAGS + " """ + SCI + "/bin/scicos.lib""";
            LDFLAGS = LDFLAGS + " """ + SCI + "/bin/scicos_f.lib""";
            LDFLAGS = LDFLAGS + " """ + SCI + "/bin/fileio.lib""";
        end
    end

    tbx_build_src(...
        ["fmucpp_wrapper", "detect_fmu_version", "allocate_fmu", "fmu_simulate_me", "fmu_simulate_cs", "fmu_callback_logger", "fmu2_simulate_me", "fmu2_simulate_cs", "fmu2_callback_logger", "fmu3_simulate_me", "fmu3_simulate_cs", "fmu3_callback_logger"], ..
        ["fmu_wrapper.cpp", "fmu_sim_utility.cpp", "fmu_simulate_me.cpp", "fmu_simulate_cs.cpp", "fmu2_simulate_me.cpp", "fmu2_simulate_cs.cpp", "fmu3_simulate_me.cpp", "fmu3_simulate_cs.cpp"], ..
        "c", ..
        src_cpp_path, ..
        "", ..
        LDFLAGS, ..
        CFLAGS, ..
        "", ..
        "", ..
        "fmucpp");
    
endfunction

builder_cpp();
clear builder_cpp; // remove builder_cpp on stack
