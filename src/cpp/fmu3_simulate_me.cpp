//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) - 2013 - Scilab Enterprises - Vladislav Trubkin
// Copyright (C) - 2017 - ESI Group - Clement DAVID
// Copyright (C) - 2024 - Dassault Systèmes - Clement DAVID
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//

#include "fmu_wrapper.hxx"

#include <algorithm>
#include <vector>

extern "C"
{
#include "api_scilab.h"
#include "Scierror.h"
#include "sciprint.h"
#include "scicos.h"
#include "scicos_block4.h"
#include "scicos_malloc.h"
#include "scicos_free.h"

#include "fmu_sim_utility.h"

    void fmu3_simulate_me(scicos_block *block, scicos_flag flag);
}

void fmu3_simulate_me(scicos_block *block, scicos_flag flag)
{
    unsigned int i = 0, j = 0;
    char *pathName = NULL, *resourceLocation = NULL, *identName = NULL, *instantiationToken = NULL;
    double *sciTolerance = NULL;
    int *sciInputTypes = NULL, *sciOutputTypes = NULL;
    unsigned int *sciNumberOfStates = NULL, *sciNumberOfEvents = NULL;
    fmi3Status status;
    workFMU *structFmu = (workFMU *)(*block->work);
    Fmu3ModelExchange *fmu = nullptr;
    double time = 0;
    uint32_t *sciRealRefs = NULL, *sciIntRefs = NULL, *sciBoolRefs = NULL;
    uint32_t *sciInputRefs = NULL, *sciOutputRefs = NULL;
    fmi3Float64 *rpar = NULL;
    fmi3Int32 *ipar = NULL;
    int *boolValue = NULL;
    fmi3Boolean loggingOn = fmi3False;
    fmiReal timeout = 1000, step = 0, tStart = 0, tStop = 0;
    fmiBoolean StopTimeDefined = fmiFalse;
    size_t realRefsSize, intRefsSize, boolRefsSize, outRefsSize, inpRefsSize;

    // return if an fmu has not been loaded
    if (block->nopar == 0)
    {
        return;
    }

    // settings
    pathName = Getint8OparPtrs(block, 1);          // path to the lib
    resourceLocation = Getint8OparPtrs(block, 10); // fmuResourceLocation
    identName = Getint8OparPtrs(block, 11);        // identifier
    instantiationToken = Getint8OparPtrs(block, 12); // instantiationToken
    loggingOn = *Getint8OparPtrs(block, 13);       // logger
    sciTolerance = GetRealOparPtrs(block, 16);     // relative tolerance

    // real values
    sciRealRefs = Getuint32OparPtrs(block, 6);                          // real refs
    realRefsSize = GetOparSize(block, 6, 1) * GetOparSize(block, 6, 2); // real refs size
    rpar = GetRparPtrs(block);                                          // real values
    // integer values
    sciIntRefs = Getuint32OparPtrs(block, 7);
    intRefsSize = GetOparSize(block, 4, 1) * GetOparSize(block, 7, 2);
    ipar = GetIparPtrs(block);
    // boolean values
    sciBoolRefs = Getuint32OparPtrs(block, 8);
    boolRefsSize = GetOparSize(block, 8, 1) * GetOparSize(block, 8, 2);
    boolValue = Getint32OparPtrs(block, 9); // boolean values
    // inputs
    sciInputRefs = Getuint32OparPtrs(block, 2);
    inpRefsSize = GetOparSize(block, 2, 1) * GetOparSize(block, 2, 2);
    sciInputTypes = Getint32OparPtrs(block, 3);
    // outputs
    sciOutputRefs = Getuint32OparPtrs(block, 4);
    outRefsSize = GetOparSize(block, 4, 1) * GetOparSize(block, 4, 2);
    sciOutputTypes = Getint32OparPtrs(block, 5);
    // states and events
    sciNumberOfStates = Getuint32OparPtrs(block, 14);
    assert(*sciNumberOfStates == block->nx);
    sciNumberOfEvents = Getuint32OparPtrs(block, 15);
    assert(*sciNumberOfEvents == block->ng);

    if (flag == Initialization)
    {
        // sciprint("\n");
        // sciprint("*****Initialization block [%d] Time [%f] \n",get_block_number(), get_scicos_time());

        // memory allocation for the main structure
        if ((*(block->work) = (workFMU *)scicos_malloc(sizeof(workFMU))) == NULL)
        {
            set_block_error(-16); // memory allocation error
            *(block->work) = NULL;
            return;
        }
        structFmu = (workFMU *)(*block->work);
        memset(structFmu, 0x0, sizeof(workFMU));

        // load library and create an instance
        structFmu->fmuME3 = new Fmu3ModelExchange();
        fmu = structFmu->fmuME3;
        if (fmu->create(pathName, identName) == false)
        {
            free_instance(block, -3, "Fmu3ModelExchange"); // internal error
            return;
        }
        // set fmi3Type
        structFmu->type = workFMU::ModelExchange;
        structFmu->version = workFMU::fmiV3;

        // instantiate model
        structFmu->instance = fmu->fmi3InstantiateModelExchange(identName, instantiationToken, resourceLocation, fmi3True, loggingOn, block, fmu3_callback_logger);
        if (structFmu->instance == NULL)
        {
            free_instance(block, -3, "fmi3InstantiateModelExchange");
            return;
        }
        FMU_SIM_PRINT("fmi3InstantiateModelExchange\n");
        structFmu->data.modelExchangeV3.state = structFmu->data.modelExchangeV3.Instantiated;
        // set debug logging
        if (loggingOn)
        {
            status = fmu->fmi3SetDebugLogging(structFmu->instance, loggingOn, 0, NULL);
            if (status > fmi3Warning)
            {
                free_instance(block, -3, "fmi3SetDebugLogging");
                return;
            }
        }

        // set start values
        if (fmiSet_parameters(block, flag) > 1)
        {
            return;
        }

        // enter initialization mode
        if (sciTolerance[0])
        {
            status = fmu->fmi3EnterInitializationMode(structFmu->instance, fmi3True, sciTolerance[0], tStart, fmi3False, 0.0);
        }
        else
        {
            status = fmu->fmi3EnterInitializationMode(structFmu->instance, fmi3False, 0.0, tStart, fmi3False, 0.0);
        }
        if (status > fmi3Warning)
        {
            free_instance(block, -3, "fmi3EnterInitializationMode");
            return;
        }
        FMU_SIM_PRINT("fmi3EnterInitializationMode\n");
        structFmu->data.modelExchangeV3.state = structFmu->data.modelExchangeV3.InitializationMode;


        if (fmiSet_values(block, flag, structFmu->data.modelExchangeV2.state == structFmu->data.modelExchangeV2.ContinuousTimeMode, sciInputTypes, &block->insz[2 * block->nin], inpRefsSize, sciInputRefs, block->inptr) > fmi2Warning)
        {
            // already freed
            return;
        }

        // retrieve solution at time = 0.00
        if (fmiGet_values(block, sciOutputTypes, &block->outsz[2 * block->nout], outRefsSize, sciOutputRefs, block->outptr) > fmi3Warning)
        {
            // already freed
            return;
        }

        // exit initialization mode on the first call
        status = fmu->fmi3ExitInitializationMode(structFmu->instance);
        if (status > fmi3Warning)
        {
            free_instance(block, -3, "fmi3ExitInitializationMode");
            return;
        }
        FMU_SIM_PRINT("fmi3ExitInitializationMode\n");
        structFmu->data.modelExchangeV3.state = structFmu->data.modelExchangeV3.EventMode;
    }
    else if (flag == ReInitialization)
    {
        if ((structFmu = (workFMU *)(*block->work)) == NULL)
        {
            return;
        }
        fmu = structFmu->fmuME3;

        if (fmiSet_values(block, flag, structFmu->data.modelExchangeV3.state == structFmu->data.modelExchangeV3.ContinuousTimeMode, sciInputTypes, &block->insz[2 * block->nin], inpRefsSize, sciInputRefs, block->inptr) > fmi3Warning)
        {
            // already freed
            return;
        }
        
        fmi3Boolean discreteStatesNeedUpdate;
        fmi3Boolean terminateSimulation;
        fmi3Boolean nominalsOfContinuousStatesChanged;
        fmi3Boolean valuesOfContinuousStatesChanged;
        status = fmu->fmi3UpdateDiscreteStates(structFmu->instance, &discreteStatesNeedUpdate, &terminateSimulation, &nominalsOfContinuousStatesChanged, &valuesOfContinuousStatesChanged, &structFmu->data.modelExchangeV3.nextEventTimeDefined, &structFmu->data.modelExchangeV3.nextEventTime);
        if (status > fmi3Discard)
        {
            free_instance(block, -3, "fmi3UpdateDiscreteStates");
            return;
        }
        if (discreteStatesNeedUpdate)
        {
            do_cold_restart();
        }
        else
        {
            if (structFmu->data.modelExchangeV3.state != structFmu->data.modelExchangeV3.ContinuousTimeMode)
            {
                status = fmu->fmi3EnterContinuousTimeMode(structFmu->instance);
                if (status > fmi3Discard)
                {
                    free_instance(block, -3, "fmi3EnterContinuousTimeMode");
                    return;
                }
                FMU_SIM_PRINT("fmi3EnterContinuousTimeMode\n");
                structFmu->data.modelExchangeV3.state = structFmu->data.modelExchangeV3.ContinuousTimeMode;
            }
        }
        if (terminateSimulation)
        {
            free_instance(block, -3, "terminateSimulation");
            return;
        }
        if (nominalsOfContinuousStatesChanged)
        {
            do_cold_restart();
        }
        if (valuesOfContinuousStatesChanged)
        {
            status = fmu->fmi3GetContinuousStates(structFmu->instance, block->x, block->nx);
            if (status > fmi3Discard)
            {
                free_instance(block, -3, "fmi3GetContinuousStates");
                return;
            }
        }

        if (fmiGet_values(block, sciOutputTypes, &block->outsz[2 * block->nout], outRefsSize, sciOutputRefs, block->outptr) > fmi3Warning)
        {
            // already freed
            return;
        }
    }
    else if (flag == DerivativeState)
    {
        if ((structFmu = (workFMU *)(*block->work)) == NULL)
        {
            return;
        }
        fmu = structFmu->fmuME3;

        // Derivative computation is always in Continuous mode
        if (structFmu->data.modelExchangeV3.state != structFmu->data.modelExchangeV3.ContinuousTimeMode)
        {
            status = fmu->fmi3EnterContinuousTimeMode(structFmu->instance);
            if (status > fmi3Discard)
            {
                free_instance(block, -3, "fmi3EnterContinuousTimeMode");
                return;
            }
            FMU_SIM_PRINT("fmi3EnterContinuousTimeMode\n");
            structFmu->data.modelExchangeV3.state = structFmu->data.modelExchangeV3.ContinuousTimeMode;
        }
        structFmu->data.modelExchangeV3.enterEventMode = fmi3False;

        // Set independent variable time
        time = get_scicos_time();
        status = fmu->fmi3SetTime(structFmu->instance, time);
        if (status > fmi3Warning)
        {
            free_instance(block, -3, "fmi3SetTime");
            return;
        }

        // Set continuous-time inputs
        if (fmiSet_values(block, flag, structFmu->data.modelExchangeV3.state == structFmu->data.modelExchangeV3.ContinuousTimeMode, sciInputTypes, &block->insz[2 * block->nin], inpRefsSize, sciInputRefs, block->inptr))
        {
            // already freed
            return;
        }

        // Set continuous-time states
        status = fmu->fmi3SetContinuousStates(structFmu->instance,  block->x, block->nx);
        if (status > fmi3Discard)
        {
            free_instance(block, -3, "fmi3SetContinuousStates");
            return;
        }

        // Evaluate and get derivatives
        status = fmu->fmi3GetContinuousStateDerivatives(structFmu->instance, block->xd, block->nx);
        if (status > fmi3Discard)
        {
            free_instance(block, -3, "fmi3GetContinuousStateDerivatives");
            return;
        }

        // Complete integrator step and return enterEventMode
        fmi3Boolean terminateSimulation = fmi3False;
        status = fmu->fmi3CompletedIntegratorStep(structFmu->instance, fmi3True, &structFmu->data.modelExchangeV3.enterEventMode, &terminateSimulation);
        if (status > fmi3Warning)
        {
            free_instance(block, -3, "fmi3CompletedIntegratorStep");
            return;
        }
        if (terminateSimulation)
        {
            free_instance(block, -3, "fmi3CompletedIntegratorStep");
            return;
        }
    }
    else if (flag == OutputUpdate)
    {
        // sciprint("\n");
        // sciprint("*****Output update block [%d] at time: %f\n", get_block_number(), get_scicos_time());
        if ((structFmu = (workFMU *)(*block->work)) == NULL)
        {
            return;
        }
        fmu = structFmu->fmuME3;

        // Set independent variable time
        time = get_scicos_time();
        status = fmu->fmi3SetTime(structFmu->instance, time);
        if (status > fmi3Warning)
        {
            free_instance(block, -3, "fmi3SetTime");
            return;
        }

        // From the previous fmi3NewDiscreteStates call, the FMU requested to enter into EventMode at a specific time
        structFmu->data.modelExchangeV3.timeEvent = (structFmu->data.modelExchangeV3.nextEventTimeDefined) && (structFmu->data.modelExchangeV3.nextEventTime <= time);

        // From Continuous-Time Mode, the FMU requested to enter into EventMode
        //  - with the enterEventMode output of fmi3CompletedIntegratorStep coming from the previous DerivativeState
        //  - with the eventInfo.nextEventTimeDefined output of fmi3NewDiscreteStates
        if (structFmu->data.modelExchangeV3.state != structFmu->data.modelExchangeV3.EventMode)
        {
            if (structFmu->data.modelExchangeV3.enterEventMode || structFmu->data.modelExchangeV3.timeEvent)
            {
                status = fmu->fmi3EnterEventMode(structFmu->instance);
                if (status > fmi3Warning)
                {
                    free_instance(block, -3, "fmi3EnterEventMode");
                    return;
                }
                FMU_SIM_PRINT("fmi3EnterEventMode\n");
                structFmu->data.modelExchangeV3.state = structFmu->data.modelExchangeV3.EventMode;
            }
        }

        // Set continuous-time and discrete-time inputs
        if (fmiSet_values(block, flag, structFmu->data.modelExchangeV3.state == structFmu->data.modelExchangeV3.ContinuousTimeMode, sciInputTypes, &block->insz[2 * block->nin], inpRefsSize, sciInputRefs, block->inptr) > fmi3Warning)
        {
            // already freed
            return;
        }

        if (structFmu->data.modelExchangeV3.state == structFmu->data.modelExchangeV3.ContinuousTimeMode)
        {
            // Set continuous-time states
            status = fmu->fmi3SetContinuousStates(structFmu->instance, block->x, block->nx);
            if (status > fmi3Discard)
            {
                free_instance(block, -3, "fmi3SetContinuousStates");
                return;
            }
        }

        // get outputs
        if (fmiGet_values(block, sciOutputTypes, &block->outsz[2 * block->nout], outRefsSize, sciOutputRefs, block->outptr) > fmi3Warning)
        {
            // already freed
            return;
        }
    }
    else if (flag == StateUpdate)
    {
        if ((structFmu = (workFMU *)(*block->work)) == NULL)
        {
            return;
        }
        fmu = structFmu->fmuME3;

        // after a scicos "root found" event ; increment super dense time: done by the solver
        if (structFmu->data.modelExchangeV3.state != structFmu->data.modelExchangeV3.EventMode)
        {
            status = fmu->fmi3EnterEventMode(structFmu->instance);
            if (status > fmi3Warning)
            {
                free_instance(block, -3, "fmi3EnterEventMode");
                return;
            }
            FMU_SIM_PRINT("fmi3EnterEventMode\n");
            structFmu->data.modelExchangeV3.state = structFmu->data.modelExchangeV3.EventMode;
        }

        // in Event-mode, there might be a point-fix iteration on all FMUs if newDiscreteStatesNeeded == fmi3True.
        // until all FMUs newDiscreteStatesNeeded == fmi3False:
        //  - set inputs
        //  - get the outputs
        //  - fmi3UpdateDiscreteStates
        fmi3Boolean discreteStatesNeedUpdate;
        fmi3Boolean terminateSimulation;
        fmi3Boolean nominalsOfContinuousStatesChanged;
        fmi3Boolean valuesOfContinuousStatesChanged;
        status = fmu->fmi3UpdateDiscreteStates(structFmu->instance, &discreteStatesNeedUpdate, &terminateSimulation, &nominalsOfContinuousStatesChanged, &valuesOfContinuousStatesChanged, &structFmu->data.modelExchangeV3.nextEventTimeDefined, &structFmu->data.modelExchangeV3.nextEventTime);
        if (status > fmi3Discard)
        {
            free_instance(block, -3, "fmi3UpdateDiscreteStates");
            return;
        }
        if (discreteStatesNeedUpdate)
        {
            do_cold_restart();
        }
        if (terminateSimulation)
        {
            free_instance(block, -3, "terminateSimulation");
            return;
        }
        if (nominalsOfContinuousStatesChanged)
        {
            do_cold_restart();
        }
        if (valuesOfContinuousStatesChanged)
        {
            status = fmu->fmi3GetContinuousStates(structFmu->instance, block->x, block->nx);
            if (status > fmi3Discard)
            {
                free_instance(block, -3, "fmi3GetContinuousStates");
                return;
            }
        }
    }
    else if (flag == ZeroCrossing)
    {
        if ((structFmu = (workFMU *)(*block->work)) == NULL)
        {
            return;
        }
        fmu = structFmu->fmuME3;

        // Zero-crossing detection is always on continuous mode, we have to set the states values
        if (structFmu->data.modelExchangeV3.state != structFmu->data.modelExchangeV3.ContinuousTimeMode)
        {
            status = fmu->fmi3EnterContinuousTimeMode(structFmu->instance);
            if (status > fmi3Warning)
            {
                free_instance(block, -3, "fmi3EnterContinuousTimeMode");
                return;
            }
            FMU_SIM_PRINT("fmi3EnterContinuousTimeMode\n");
            structFmu->data.modelExchangeV3.state = structFmu->data.modelExchangeV3.ContinuousTimeMode;
        }

        // Set independent variable time
        time = get_scicos_time();
        status = fmu->fmi3SetTime(structFmu->instance, time);
        if (status > fmi3Warning)
        {
            free_instance(block, -3, "fmi3SetTime");
            return;
        }

        // Set continuous-time and discrete-time inputs
        if (fmiSet_values(block, flag, structFmu->data.modelExchangeV3.state == structFmu->data.modelExchangeV3.ContinuousTimeMode, sciInputTypes, &block->insz[2 * block->nin], inpRefsSize, sciInputRefs, block->inptr) > fmi3Warning)
        {
            // already freed
            return;
        }

        // Set continuous states
        status = fmu->fmi3SetContinuousStates(structFmu->instance, block->x, block->nx);
        if (status > fmi3Discard)
        {
            free_instance(block, -3, "fmi3SetContinuousStates");
            return;
        }

        // Get event indicators to detect the zero-crossing
        status = fmu->fmi3GetEventIndicators(structFmu->instance, block->g, block->ng);
        if (status > fmi3Discard)
        {
            free_instance(block, -3, "fmi3GetEventIndicators");
            return;
        }
    }
    else if (flag == Ending)
    {
        // sciprint("\n");
        // sciprint("*****Ending block [%d] \n", get_block_number());
        if ((structFmu = (workFMU *)(*block->work)) == NULL)
        {
            return;
        }
        fmu = structFmu->fmuME3;

        if (structFmu->data.modelExchangeV3.state > structFmu->data.modelExchangeV3.InitializationMode)
        {
            time = get_scicos_time();
            status = fmu->fmi3SetTime(structFmu->instance, time);
            if (status > fmi3Warning)
            {
                free_instance(block, -3, "fmi3SetTime");
                return;
            }

            // terminate simulation
            status = fmu->fmi3Terminate(structFmu->instance);
            if (status > fmi3Warning)
            {
                free_instance(block, -3, "fmi3Terminate");
                return;
            }

            if (fmiGet_values(block, sciOutputTypes, &block->outsz[2 * block->nout], outRefsSize, sciOutputRefs, block->outptr) > fmi3Warning)
            {
                // already freed
                return;
            }
        }

        // freeing of instance
        fmu->fmi3FreeInstance(structFmu->instance);
        free_instance(block, 0, "fmu_simulate_me");
        return;
    }
}
