//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) - 2014 - Scilab Enterprises - Vladislav Trubkin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
clear check_results;
function check_results(refs, outputs, outName, solver)
    outName = ["time"; outName];
    headers = ["", "Mean error", "Standard deviation", "Min", "Max", "Order"]

    // check the results with the reference results
    function ret = calc_err(i, refs, outputs)
        xp = refs.time;
        x = outputs.time;
        y = outputs.values(:,i);

        dk = splin(x, y);
        yp = interp(xp, x, y, dk);

        err = abs(refs.values - yp);
        m = mean(err, 'r');
        
        ret = [m, stdev(err, 'r', m)]
    endfunction

    outMin = min(outputs.values, 'r');
    outMax = max(outputs.values, 'r');

    err = zeros(size(outName, "r")-1, size(headers, 'c')-1)
    for i = 1:size(outName, "r")-1;
        err(i, :) = [calc_err(i, refs, outputs) outMin(i) outMax(i) outputs.values($, i)];
    end

    res = [headers; ([outName(2:$) , string(err)])];
    disp("Type of solver: " + solver);
    disp(res);
endfunction
