read-description:
  stage: .pre
  image: busybox
  tags: [x86_64-linux-gnu, docker, scilab]
  script:
    - awk "/Toolbox:/{print \"TBX_NAME=\"\$2}" DESCRIPTION  |tee ./build.env 
    - awk "/^Version:[ ]*/{print \"TBX_VERSION=\"\$2}" DESCRIPTION |tee -a ./build.env
    - source ./build.env
    - | # check if DESCRIPTION Version: match the git tag
      if [ ! -z "${CI_COMMIT_TAG}" ] && [ "${CI_COMMIT_TAG}" != "${TBX_VERSION}" ]; then
        echo "CI_COMMIT_TAG ${CI_COMMIT_TAG} does not match DESCRIPTION Version: ${TBX_VERSION}"
        exit -1
      fi;
     
  artifacts:
    reports:
      dotenv: build.env

source-archive:
  stage: build
  image: busybox
  tags: [x86_64-linux-gnu, docker, scilab]
  variables:
    TBX: "$TBX_NAME-$TBX_VERSION-$CI_PIPELINE_IID-src"
  script:
    - echo $TBX
    - mkdir ../$TBX && cp -a -t ../$TBX/ * && mv ../$TBX ./
    - tar czf $TBX.tar.xz $TBX
  artifacts:
    paths:
      - "$TBX.tar.xz"

x86_64-linux-gnu_build:
  stage: build
  image: registry.gitlab.com/scilab/scilab/linux-runner:$SCI_VERSION
  tags: [x86_64-linux-gnu, docker, scilab]
  variables:
    TBX: "$TBX_NAME-$TBX_VERSION-$CI_PIPELINE_IID-bin-linux"
  script:
    - |
      scilab -nw <<EOF
      try
        tbx_make(pwd())

        // download tbx_package.sci as it may not be available
        [result, status] = http_get("https://gitlab.com/scilab/scilab/-/raw/abfb1d81e87406d1be3f6faa757d4e990d95e7c9/scilab/modules/modules_manager/macros/tbx_package.sci");
        assert_checkequal(status, 200);
        execstr(result);

        package_file = tbx_package(pwd(), "$CI_PIPELINE_IID")

        [status, message] = movefile(package_file, "$TBX-for-$SCI_VERSION.tar.xz")
        exit(0);
      catch
        [__msg__, __err__] = lasterror()
        exit(__err__)
      end
      EOF
  artifacts:
    paths:
      - "$TBX-for-$SCI_VERSION.tar.xz"
  parallel:
    matrix: &SCI_VERSION
      - SCI_VERSION: ["2024.0.0", "2024.1.0", "2025.0.0"]

x64_windows_build:
  stage: build
  tags: [x64_windows, scilab, shell]
  variables:
    TBX: "$TBX_NAME-$TBX_VERSION-$CI_PIPELINE_IID-bin-windows"
  script:
    - |
      echo @"
      try

        tbx_make(pwd())

        // download tbx_package.sci as it may not be available
        [result, status] = http_get("https://gitlab.com/scilab/scilab/-/raw/abfb1d81e87406d1be3f6faa757d4e990d95e7c9/scilab/modules/modules_manager/macros/tbx_package.sci");
        assert_checkequal(status, 200);
        execstr(result);

        package_file = tbx_package(pwd(), "$CI_PIPELINE_IID")

        [status, message] = movefile(package_file, "$TBX-for-$SCI_VERSION.zip")
        exit(0);
      catch
        [__msg__, __err__] = lasterror()
        exit(__err__)
      end
      "@ | &"E:/nosave/work/releases/scilab-$SCI_VERSION/bin/scilab" -nw
  artifacts:
    paths:
      - "$TBX-for-$SCI_VERSION.zip"
  parallel:
    matrix: *SCI_VERSION

x86_64-linux-gnu_test:
  stage: test
  image: registry.gitlab.com/scilab/scilab/linux-runner:$SCI_VERSION
  tags: [x86_64-linux-gnu, docker, scilab]
  variables:
    GIT_STRATEGY: none
    TBX: "$TBX_NAME-$TBX_VERSION-$CI_PIPELINE_IID-bin-linux"
  needs:
    - read-description
    - job: x86_64-linux-gnu_build
      parallel:
        matrix: *SCI_VERSION
  script:
    - ARCH=$(cc -print-multiarch)
    - rm -fr results-for-$SCI_VERSION
    - mkdir --parents results-for-$SCI_VERSION/$ARCH
    - |
      xvfb-run scilab -nwni <<EOF
      try

        files = decompress("$TBX-for-$SCI_VERSION.tar.xz")
        test_run('./$TBX_NAME-$TBX_VERSION', [], [], fullfile(pwd(), 'results-for-$SCI_VERSION', '$ARCH', '$TBX.xml')),
        
        deletefile(files);
        exit(0);
      catch
        [__msg__, __err__] = lasterror()
        exit(__err__)
      end
      EOF
  artifacts:
    when: always
    paths:
      - "results-for-$SCI_VERSION"
    reports:
      junit: "results-for-$SCI_VERSION/*/$TBX.xml"
  parallel:
    matrix: *SCI_VERSION

x64_windows_test:
  stage: test
  tags: [x64_windows, scilab, shell]
  variables:
    GIT_STRATEGY: none
    TBX: "$TBX_NAME-$TBX_VERSION-$CI_PIPELINE_IID-bin-windows"
  needs:
    - read-description
    - job: x64_windows_build
      parallel:
        matrix: *SCI_VERSION
  script:
    - $ARCH="x64"
    - if (Test-Path results-for-$SCI_VERSION) { Remove-Item -Recurse -Force results-for-$SCI_VERSION }
    - New-Item -ItemType directory results-for-$SCI_VERSION\$ARCH-windows
    - |
      echo @"
      try

        files = decompress("$TBX-for-$SCI_VERSION.zip")
        test_run('./$TBX_NAME-$TBX_VERSION', [], [], fullfile(pwd(), 'results-for-$SCI_VERSION', '$ARCH-windows', '$TBX.xml'))
        
        deletefile(files);
        exit(0);
      catch
        [__msg__, __err__] = lasterror()
        exit(__err__)
      end
      "@ | &"E:/nosave/work/releases/scilab-$SCI_VERSION/bin/scilab" -nwni
  artifacts:
    when: always
    paths:
      - "results-for-$SCI_VERSION"
    reports:
      junit: "results-for-$SCI_VERSION/*/$TBX.xml"
  parallel:
    matrix: *SCI_VERSION

# generate an archive of all tests files and logs
archive_test_results:
  stage: deploy
  image: registry.gitlab.com/scilab/scilab/linux-runner:2025.0.0
  tags: [x86_64-linux-gnu, docker, scilab]
  variables:
    GIT_STRATEGY: none
    TBX: "$TBX_NAME-$TBX_VERSION-$CI_PIPELINE_IID-bin-linux"
  script:
    - |
      scilab -nwni <<EOF
      mode(1)

      dirs = ls(ls("results-for-*") + "/*");
      results = ls(dirs + "/*.xml");

      csv = emptystr(size(dirs, 1), 3);
      names = strsubst(part(dirs, (length("results-for-")+1):$), filesep(), " ");
      for i=1:size(results, 1)
          csv(i, 1:3) = xmlGetValues("//testsuites/testsuite", ["tests", "errors", "failures"], results(i));
      end

      m = matrix2table(csv, "VariableNames", ["tests", "errors", "failures"], "RowNames", names);
      writetable(m, "archive_test_results.csv", "WriteVariableNames", %t, "WriteRowNames", %t);
      disp(m)
      exit(0)
      EOF
  artifacts:
    paths:
      - "archive_test_results.csv"
      - "results-for-*/*/*.xml"
      - "results-for-*/*/*.tst"
      - "results-for-*/*/*.res"
      - "results-for-*/*/*.err"
      - "results-for-*/*/*.dia.tmp"
      - "results-for-*/*/*.dia"
